#!/bin/bash

BUILD_PLATFORMS="linux/amd64,linux/arm64"
IMAGE=registry.gitlab.com/gudenaf/tools/docker:latest

echo "Preparing build environment"
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
docker buildx create --name multiarch --driver docker-container --use

echo "Checking build environment"
docker buildx inspect --bootstrap

echo "Building $IMAGE..."
docker buildx build --platform $BUILD_PLATFORMS --tag "$IMAGE" --push .