Docker with [buildx](https://github.com/docker/buildx)

== Build

Use `build.sh` to build it locally for all supported architectures. Requires `buildx`.